const { Builder } = require('xml2js')

const DTAssoc = (items) => ({
  dt_assoc: [
    {
      item: Object.keys(items).map(key => {
        const value = items[key]

        if (value.dt_assoc) { return { $: { key }, dt_assoc: value.dt_assoc } }

        return {
          $: {
            key
          },
          _: value
        }
      })
    }
  ]
})

const xmlString = (contactInfo, domain, approver, csr) => {
  const contactSet = {
    admin: DTAssoc(contactInfo),
    billing: DTAssoc(contactInfo),
    tech: DTAssoc(contactInfo)
  }

  const attributes = {
    reg_type: 'RENEW',
    domain,
    approver_email: approver,
    contact_set: DTAssoc(contactSet)
  }

  const main = {
    protocol: 'XCP',
    action: 'sw_register',
    object: 'trust_service',
    attributes: DTAssoc(attributes),
    handle: 'process',
    csr: csr,
    period: 1,
    server_type: 'apachessl',
    product_type: 'quickssl'
  }

  const builder = new Builder({
    xmldec: {
      standalone: false
    },
    doctype: { 'sysID': 'ops.dtd' }
  })

  const obj = {
    OPS_envelope: {
      header: {
        version: '0.9'
      },
      body: {
        data_block: DTAssoc(main)
      }
    }
  }

  return builder.buildObject(obj)
}

module.exports = xmlString
