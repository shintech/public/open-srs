// const got = require('got')
const xml = require('./xml')
const http = require('./http')
const { exec } = require('child_process')

const connectionOptions = {
  live: {
    reseller_username: process.env['RESELLER_USERNAME'],
    api_key: process.env['API_KEY'],
    api_host: 'https://rr-n1-tor.opensrs.net',
    api_port: 55443
  },
  test: {
    reseller_username: process.env['RESELLER_USERNAME'] || 'reseller-username',
    api_key: process.env['API_KEY'] || 'api-key',
    api_host: 'https://horizon.opensrs.net',
    api_port: 55443
  }
}

const domain = 'example.org'
const approvalEmail = 'email@example.org'

const contactInfo = {
  country: 'US',
  org_name: 'Example, Inc.',
  phone: '+1.5555555555',
  first_name: 'Turd',
  last_name: 'Sandwich',
  address2: 'Oval Office',
  state: 'DC',
  email: 'admin@example.com',
  city: 'Washington',
  postal_code: '20500',
  fax: '+1.5555555555',
  address1: '1600 Pennsylvania Ave',
  title: 'President'
}

const { country, state, city } = contactInfo

exec(`openssl req -new -newkey rsa:1024 -nodes -subj '/C=${country}/ST=${state}/L=${city}/O=Global'`, (err, stdout, stderr) => {
  if (err) {
    console.log(err)
    return
  }

  const csr = stdout.toString()
  const xmlRequest = xml(contactInfo, domain, approvalEmail, csr)

  http(xmlRequest, connectionOptions)
})
