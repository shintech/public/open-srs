# shintech/public/openSRS

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
3. [ Usage ](#usage) <br />

<a name="synopsis"></a>
### Synopsis

Node JS SDK for OpenSRS

<a name="install"></a>
### Installation

    git clone https://gitlab.com/shintech/public/open-srs.git

<a name="usage"></a>
### Usage

Does not actually work at this time because I do not have an API key for OpenSRS and that costs real money. The xml.js file should output XML that will successfully order a RapidSSL certificate. The XML that is output by the function can be compared against a snapshot with the following code:

    yarn test:all


This test will also parse the XML to JavaScript and perform a couple of tests as well. This test can be modified to test more of the XML properties if necessary.
