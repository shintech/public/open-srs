const mock = require('./mock')

const NODE_ENV = process.env['NODE_ENV'] || 'test'
const PORT = process.env['PORT'] || 8000
const HOST = process.env['HOST'] || 'localhost'

global.process.env = {
  NODE_ENV,
  PORT,
  HOST
}

global._mock = mock
