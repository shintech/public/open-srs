const crypto = require('crypto')
const { request } = require('https')

module.exports = function (xml, connectionOptions) {
  const connectionString = (process.env['NODE_ENV'] !== 'production') ? connectionOptions.test : connectionOptions.live

  const createHash = (string) => crypto.createHash('md5').update(string + connectionString['api_key']).digest('hex')

  const hash = createHash(xml)
  const signature = createHash(hash)

  var options = {
    hostname: connectionString['api_host'],
    port: connectionString['api_port'],
    path: '/',
    method: 'POST',
    headers: {
      'Content-Type': 'text/xml',
      'X-Username': connectionString['reseller_username'],
      'X-Signature': signature
    }
  }

  request(options, function (res) {
    console.log('statusCode: ', res.statusCode)
    console.log('headers: ', res.headers)

    res.on('data', function (d) {
      process.stdout.write(d)
    })
  })

    .write(xml)
    .end()
    .on('error', function (e) {
      console.error(e)
    })
}
